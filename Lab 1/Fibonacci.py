'''@file Fibonacci.py
@brief Calculates a Fibonacci number at a specific index
@details Use the function fib() to find the fibonacci sequence of the corresponding index
@author: Nico Poirier
@date 1/14/21
'''

def fib(idx):
    '''
    @brief     This function calculates a Fibonacci number at a specific index.
    @param idx An integer specifying the index of the desired Fibonacci number
    @return The associated Fibonacci number at the specified index
    '''
    list = [0,1]
    for n in range (2,idx+1,1):
        list.append(list[n-2] + list [n-1])
        # append the previous 2 terms added together
    return list[idx]

if __name__ == '__main__': 
    while True:
        try:
            user_in = input('Please enter an index for the desired Fibonacci number: ')
            # ask user to input an index
            
            if user_in.isdigit():
            # check if user input is a digit
                idx = int(user_in)
                # turn the index into an integer if the input is an integer
                print ('Fibonacci number at index {:} is {:}'. format(idx,fib(idx))) 
                # print the corresponding fib number
            else:
                print ('Error: Index must be a positive integer')
                # if input is not a positive integer display error
            idx = 0
        except KeyboardInterrupt:
            print('Thanks!')
            break
        