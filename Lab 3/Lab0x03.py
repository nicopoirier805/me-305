# -*- coding: utf-8 -*-
"""@file Lab0x03.py
@brief A "Simon Says" game implemented on the nucleo
@details The program will prompt the user to push the User Button (B1) to start. 
            The program will explain how the game works and begin displaying an LED pattern on User LED (LD2).  
            The user will be prompted to enter their response. If the response is correct, 
            the next part of the pattern will be displayed. If the response is incorrect, 
            a message will be displayed and a loss will be recorded. If the user makes it to 
            the end of the pattern, a message will be displayed and a win will be recorded. 
            Total wins and losses will be displayed after each win or loss. 
            
            @image html Lab0x03_diagram.png width=50%
            
            
            
@author: Nico Poirier
@date 2/4/21
"""

# from simon_says_task import simon_says_task
import utime
import pyb
#from random import shuffle

def onButtonPressFCN(IRQ_src):
    '''@brief Detects if the button is pressed or released
    @param IRQ_src 
    @return True
    '''
    global button_pushed
    global start_button # start button is used to initiate game
    if start_button == 0:
        start_button = 1
    button_pushed = True
    
#def shuffle_list(mylist):
    #mylist = shuffle(mylist)

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    
    count = 0
    state = 0
    wins = 0
    losses = 0
    n2 = 0
    n = 0
    button_pushed = False
    start_button = 0 #start button starts at 0, 1 means the loop runs, 2 means wait to be set back to 0 upon reinitiation
    print('Welome! Press blue button to play the game')
    
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    t2ch1.pulse_width_percent(0) # set led intially off
    
    # Create a pin object for PC13
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    # Associate the callback function with the pin by setting up an external interrupt.
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_RISING_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    
    timelist = [0,0,0,0,0,0]
    LED_list = [1000,500,1500,1000,2000,1000,1000]
    current_time = 0
    previous_time = 0
    tolerance = 500 # set the tolerance for margin of error in ms
    
    while True:
        try:
            if start_button == 1:
                #shuffle_list(LED_list)
                start_button = 2
                state = 1
                
            elif state == 1:
                print('Pattern will be displayed in:')
                utime.sleep_ms(200)
                print('3...')
                utime.sleep_ms(200)
                print('2...')
                utime.sleep_ms(200)
                print('1...')
                utime.sleep_ms(200)
                  
                
                for n in range(2*count+1):
                    print('n: ' + str(n))
                    print(n%2)

                    if n % 2 == 0: # if n is even or 0
                        t2ch1.pulse_width_percent(100 * ((n % 2)+1)) #led full brightness (even) led off (odd)
                        print('LED on')
                        utime.sleep_ms(LED_list[n]) # wait for the corresponding time
                        t2ch1.pulse_width_percent(0)
                        print('count:' + str(count))
                        state = 2
                    
                    else:
                        t2ch1.pulse_width_percent(0) # turn led off
                        print('LED off')
                        utime.sleep_ms(LED_list[n]) #wait for the corresponding time                        
                        state = 2
                        
                           
            elif state == 2:
                print('Please enter response')
                idx = 0
                button_pushed = False
                while idx <= count + 1:
        
                    if button_pushed:
                        previous_time = current_time
                        current_time = utime.ticks_ms()
                        timelist[idx] = utime.ticks_diff(current_time, previous_time) #set the list value to the time button is pushed or released
                        print('user response: ' + str(timelist[idx]))
                        print('correct response: ' + str(LED_list[idx]))
                        idx += 1 #add 1 to idx
                        button_pushed = False
                state = 3
            
            elif state == 3:
                
                if count == 2:
                    print('You Win!')
                    wins += 1
                    state = 4
                    utime.sleep_ms(1000)
                        
                else:
                
                    if n2 <= count + 1:
                        
                        real = LED_list[n]
                        user = timelist[n+1]
                        
                        if n == 5: # if n is equal to the length of the list
                            print('You Win!')
                            wins += 1
                            state = 4
                            utime.sleep_ms(1000)
                            
                        elif user >= (real - tolerance) and user <= (real + tolerance):
                            state = 1
                            count += 1
                            print(timelist)
                            print(LED_list)
                            print('Correct response was entered')
                            utime.sleep_ms(1000)
                            n2 += 1
                    
                        else:
                            print('You lost.')
                            print(timelist)
                            print(LED_list)
                            losses += 1
                            state = 4
                            utime.sleep_ms(1000)
                        
            elif state == 4:
                print('Wins: ' + str(wins) + ' Losses: ' + str(losses))
                utime.sleep_ms(1000)
                start_button = 0
                state = 0
                n = 0
                n2 = 0
                count = 0
                print('Press the button to play again.')
                
                        
                
            else:
                pass
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('You have manually exited the game')
            break
                
    # Program de-initialization goes  here
    print('Thanks for playing!')
    
    