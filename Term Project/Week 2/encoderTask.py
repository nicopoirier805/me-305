# -*- coding: utf-8 -*-
"""@file encoderTask.py
@brief     This file updates the encoder at the requisite interval to avoid missing any encoder ticks. 
@details   This file defines an encoder, using pin B6, pin B7, time 4, and a period of 100.
            Task1() calls encoder.update for the defined encoder. Task2() is designed to 
            take in a set of commands. If z is pressed, the encoder is zeroed. If p is pressed, 
            the encoder position is printed. If d is pressed, the encoder delta is printed.
            If g is pressed, collect encoder 1 data for 30 seconds and generate a plot. 
            If s is pressed, data collection is ended immediatley. 
@author: Nico Poirier
@date 3/1/21
"""

from encoder import encoder
import pyb

pinB6   = pyb.Pin(pyb.pin.cpu.B6)
pinB7   = pyb.Pin(pyb.pin.cpu.B7)
timer   = pyb.Timer(4, prescaler = 0, period = 0xFFFF)
Period  = 100
enc = encoder(pinB6, pinB7, timer, Period)
        
if __name__ == "__main__":
    # update the encoder object at the requisite interval,
    # to avoid missing any encoder ticks
    task1 = encoder.update(enc)
    
    # handle serial communication between PC frontend and backend running on nucleo
    # wait for an input of z, p, d, g, or s
    # once the input is recieved, complete corresponding task
    # task2 = 
    
    while True:
        try:
            
            task1.run()
            
            
            
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break    
        