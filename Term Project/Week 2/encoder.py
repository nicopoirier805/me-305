"""@file encoder.py
@brief          Implements the encoder class
@details        An optical encoder is a common way of measuring movement of a motor.
                The STM32 processors are able to read the high frequencies that encoders
                output. Encoders take note of state change and can be used to detect motion
                when the states change. A change from (0,0) to (0,1) indicates movement
                to the right and a change from (0,1) to (0,0) indicates movement to the left.
                This file implements a class for an optical quadrature encoder.
@date 3/1/21
@author: Nico Porier
"""

import pyb
import utime
import shares

class encoder:
    ''' This class encapsulates the operation of the timer to read from an encoder
    '''
    
    def __init__ (self, p1, p2, timer, Period):
        '''The class includes a constructor __init__() which does the setup, and methods
        update(), get_position(), set_posititon(), get_delta(), and update(). 
        @param p1 A pin on the nucleo that will be assigned when encoder() called
        @param p2 A pin on the nucleo that will be assigned when encoder() is called
        @param timer A timer on the nucleo that will be assigned when encoder() is called
        @param Period The time the class will wait to run again
        '''
        # define arbitrary pins
        # these pins will be input in another file when calling encoder()
        self.p1         = p1
        self.p2         = p2
        
        # define arbitrary timer
        # this timer will be input in anoother file when calling encoder()
        # timer = 
        self.timer      = timer
        
        # define period as an arbitrary number to be set in another file
        # period is the amount of time the program will wait between runs
        #when encoder() is called
        self.Period     = Period
        
        # this time is the current time in ms
        self.myTime     = utime.ticks_ms()
        # next time specifies the next time run should be called
        self.nextTime   = utime.ticks_add(self.myTime, Period)
        
        self.theta      = 0
        self.delta      = 0
        self.myuart     = pyb.UART(2)
        
        # set up channel 1 and 2
        # timer is specified when encoder() is called
        # ch1 will be set to p1 which is specified when encoder() is called
        # ch2 will be set to p2 which is specified when encoder() is called
        # encoder is configured in Encoder mode:
            #the counter changes when when ch1 or ch2 changes
        self.ch1        = self.timer.channel(1,self.timer.ENC_AB, pin = self.p1)
        self.ch2        = self.timer.channel(2,self.timer.ENC_AB, pin = self.p1)
        
        # get counter
        self.count = self.timer.counter()
        
    def run (self):
        '''@brief Runs one iteration of the task
        '''
        current_time    = utime.ticks_ms()
        
        # the program will run once currentTime is >= next time,
        # for this lab 100 is used
        if utime.ticks_diff(current_time, self.nextTime) >= 0:
            self.update()
            self.myTime = utime.ticks_ms()
            
    def update (self):
        '''@brief When called regularly, updates the recorded 
        position of the encoder
        '''
        new_ct = self.timer.counter()
        # find the difference between the new count and the old count 
        self.delta = new_ct - self.count
        
        
        if self.delta   >= 0xFFFF:
            self.delta  -= 0xFFFF
            
        elif self.delta <= 0xFFFF:
            self.delta  += 0xFFFF
            
        elif self.delta > -0xFFFF/2 and self.delta < 0xFFFF/2:
            pass
        
        self.theta += self.delta
        
        # send encoder position to shares file
        shares.enc_pos = self.theta
        
        # get counter
        self.count = self.timer.counter()
        
    def get_position(self):
        '''@brief Returns the most recently updated position of the encoder
        '''
        return(self.theta)
    
    def set_position(self, position):
        '''@brief Resets the position to a specified value
        @param position A specified value
        '''
        self.theta = position
        
    def get_delta(self):
        '''@brief Returns the difference in recorded position between the 
        two most recent calls to update()
        '''
        return(self.delta)
        
        
        
        
        
            
        
        
        
        