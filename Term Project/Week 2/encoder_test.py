"""@file encoder_test.py
@brief
@details
@date 3/1/21
@author: Nico Poirier
"""

import pyb
from encoder import encoder
from pyb import UART, Pin, Timer

pyb.repl_uart(None)

myuart  = UART(2)
pinB6   = Pin(Pin.cpu.B6)
pinB7   = Pin(Pin.cpu.B7)
Period  = 100

timer   = Timer(4, prescaler = 0, period = 0xFFFF)
enc     = encoder.mot_Enc(pinB6, pinB7, timer, 100)

while True:
    try: 
        enc.run()
        if myuart.any() != 0:
            
            val = myuart.readchar()
            
            # if z is pressed set position to 0
            if val == 122:
                enc.set_position(0)
            
            # if p is pressed, get position and send it 
            elif val == 112:
                print(enc.get_position())
                myuart.write(str(enc.get_delta()))
            
            # if d is pressed, get delta and send it
            elif val == 100:
                print(enc.get_delta())
                myuart.write(str(enc.get_delta))
                
            else:
                pass
            
        else:
            pass
        
    except KeyboardInterrupt:
        # This except block catches "Ctrl-C" from the keyboard to end the
        # while(True) loop when desired
        print('Ctrl-c has been pressed')
        break
        

