# -*- coding: utf-8 -*-
"""@file main.py
@brief          Data collection task used to call the function UI_dataGen()
@details        When 'g' is pressed, this task will begin data collection and 
                will begin sampling from the equation y(t) = exp(-t/10)*sin(2*pi*t/3).
                Data will be stores on the nucleo for batch transmission back to the laptop.
@date 2/18/21
@author: Nico
"""

from UI_dataGen import UI_dataGen


Task1 = UI_dataGen(0, 100, True)


while True:
    Task1.run()
        