# -*- coding: utf-8 -*-
"""@file main.py
@brief Will begin sampling data upon reciept of of 'g' or 'G'
@details (Data will be sampled from the equation y(t) = e^-t/10 * sin (2*pi*t/3) 
          and data will be stored on the Nucleo for batch transmission back to the laptop.)
@date 2/18/21
@author: Nico
"""


import math
from array import array
from pyb import UART
import utime

myuart = UART(2)

# while True:
#     if myuart.any() != 0:
#         val = myuart.readchar()
#         myuart.write('You sent an ASCII ' + str(val) + 'to the Nucleo')

if __name__ == '__main__': 
    state = 0
    
    # set arrays to float as the data type by specifying 'f'
    # set up array for time
    times = array('f', 3001*[0])
    
    #set up array to store data values
    values = array('f', 3001*[0])
    
    n = 0 # keeps track of index for times and values
    print_n = 0 # keeps track of sending the values to python
    
    while True:
        try:
            if state == 0:
                if myuart.any():
                    val = myuart.readchar()
                    if val == 103 or val == 71:
                        state = 1
                        base_time = utime.ticks_ms()
                    
                    else:
                        pass
                    
                else:
                    pass
                        
                
            elif state == 1:
                
                # if s is pressed finish collecting data and jump to state 2
                if myuart.any():
                    val = myuart.readchar()
                    
                    if val == 115 or val == 83:
                        state = 2
                    
                    else:
                        pass
                
                # if s is not pressed continue collecting data
                elif n < len(times):
                    mytime = utime.ticks_ms()
                    current_time = utime.ticks_diff(mytime, base_time)
                    
                    if current_time >= n*100:
                        n_adjust = n*.1
                        times[n] = n_adjust
                        values[n] = math.exp(-times[n]/10) * math.sin(2*math.pi*times[n]/3)
                        n += 1
                        
                        
                    else:
                        pass
                
                # if the lists are full, jump to state 2
                else: 
                    state = 2                    
                    
            elif state == 2:
                # send data back to spyder front end
                for n in range(len(times)):
                    # print(str(times[n]) + ', ' + str(values[n]))
                    print('{:}, {:}'.format(times[n], values[n]))      
                state = 3
                
            elif state == 3:
                print('Data has been sent')
                # reset array for time
                times = array('f', 3001*[0])
                # reset array to store data values
                values = array('f', 3001*[0])
                #need to figure out how to reset val
                state = 0
                n = 0
                        
            else:
                pass
                
                               
            
            
        except KeyboardInterrupt:
            print('Thanks!')
            break
        
    print('Thanks')