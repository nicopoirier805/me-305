# -*- coding: utf-8 -*-
"""@file UI_dataGen.py
@brief      Implements the UI_dataGen class
@author: Nico Poirier
@date 2/18/21
"""

import math
from array import array
from pyb import UART
import utime




class UI_dataGen:
    '''Data generation class that can be called upon in another file
        This class allows the nucleo to talk to the computer. Characters
        are sent from the computer when a key is pressed. If "g" or "G" is 
        typed, the class initiates and begins sampling data from the equation 
        y(t) = exp(-t/10)*sin(2*pi*t/3). The data will be stored on the nucleo
        and eventually sent back to the computer to be graphed. Sampling will 
        proceed for 30 seconds before the data is sent to the computer in (x,y) 
        format.
    '''
    
    # Static variables are defined outside of methods
    
    ## State 1 constant
    S0_INIT          = 0
    S1_CHECK_CHAR   = 1
    S2_COLLECT_DATA  = 2
    S3_SEND_DATA  = 3
    
    def __init__(self, taskNum, period, DBG_flag=True):
        ## The current state of the finite state machine
        self.state = 0
                
        ## Flag to specify if debug messages print
        self.DBG_flag = DBG_flag
        
        ## Timestamp for last iteration of the task in microseconds
        #  could eliminate for concision if desired
        self.lastTime = utime.ticks_ms()
        
        ## The period for the task in microseconds
        self.period = period
        
        ## Timestamp for the next iteration of the task
        self.nextTime = utime.ticks_add(self.lastTime, self.period)
        
        ## An integer identifying the task
        self.taskNum = taskNum
        
        # not sure if I need this self.thisTime = utime.ticks_us()
        
        self.myuart = UART(2)
        
        self.char = ''
        self.n = 0
        self.times = array('f', 301*[0])
        self.values = array('f', 301*[0])
        self.basetime = 0
        self.idx = 0
        
        
    def run(self):
        '''@brief Runs one iteration of the task
        @details Each time the task is run, the current time is updated. The 
        difference between current time and the time that "g" was pressed is 
        calculated. This time difference is plugged into the equation to 
        generate a data point. These data points are saved to be sent to the pc later.
        '''
        
        # update time each time this is run
        thisTime = utime.ticks_ms()
        
        if utime.ticks_diff(thisTime, self.nextTime) >= 0:
            ## Timestamp for the next iteration of the task
            self.nextTime = utime.ticks_add(self.nextTime, self.period)
            # if self.DBG_flag:
            #     #print('T: ' + str(self.taskNum) + 'S: ' + str(self.state) + "R: " + str(self.runs))
            #     print('time: ' + str(self.times[self.n]) + "values: " + str(self.values[self.n]))
            
            if self.state == self.S0_INIT:
                #run state 0 code; initialize what u need for all of ur states
                self.transitionTo(self.S1_CHECK_CHAR)
                
            elif self.state == self.S1_CHECK_CHAR :
                #run state 1 code, repeadedly check if anything is sent from the front end

                if self.myuart.any():
                    self.char = self.myuart.readchar()
                    print(self.char)
                
                    if self.char == 103 or self.char == 71:
                        # if g or G is pressed, start data collection
                        self.transitionTo(self.S2_COLLECT_DATA)
                
                
                
            elif self.state == self.S2_COLLECT_DATA:                   
                # repeadedly check if anything is sent from the front end                
                if self.myuart.any():
                   self.char = self.myuart.readchar()
               
                   if self.char == 115 or self.char == 83:
                       # if s or S is pressed, cancel data collection
                       self.transitionTo(self.S3_SEND_DATA)
                   
                elif self.n < len(self.times):
                    # if s is not pressed continue collecting data
                    self.times[self.n] = self.n* .1
                    self.values[self.n] = math.exp(-self.times[self.n]/10) * math.sin(2*math.pi*self.times[self.n]/3)
                    #print('time: ' + str(self.times[self.n]) + "values: " + str(self.values[self.n]))
                    self.n += 1
                    
                else:
                    self.transitionTo(self.S3_SEND_DATA)
                    
            elif self.state == self.S3_SEND_DATA:
                # send data one at a time
                for self.idx in range(len(self.times)):
                    # Nucleo sends data to the PC using UART.write
                    self.myuart.write('{:}, {:}'.format(self.times[self.idx], self.values[self.idx])) 
                    # print for debugging purposes
                    print('{:}, {:}'.format(self.times[self.idx], self.values[self.idx])) 
                # after data has been transfered go back to state 1
                self.transitionTo(self.S1_CHECK_CHAR)
                
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here   
                
            
    def transitionTo(self, newState):
        if self.DBG_flag:
            print(str(self.state) + "->" + str(newState))
        self.state = newState
             
            
        