# -*- coding: utf-8 -*-
"""@file Lab0xFF.py
@brief UI front end that ask user for an input and begins sampling data (If the user presses 'g',
        the Nucleo will begin sampling data. The user can stop data collection
        at any point by pressing 's'.)
@details (Data will be sampled from the equation y(t) = e^-t/10 * sin (2*pi*t/3)
          If the user presses 'g', the Nucleo will begin sampling data.
          The user can stop data collectionat any point by pressing 's'.)
@author: Nico Poirier
@date2/18/21
"""

# set up the serial port, the baud rate, and the timeout flag
import serial
ser = serial.Serial(port='COM3',baudrate=115273,timeout=1)

#send an ASCII character
def sendChar():
    '''@brief Prompts the user for an input and writes it to serial
    @details After the input is recieved, it is sent as an ACII. The 
            ASCII will later be sent back to the computer and to be 
            decoded.
    '''
    inv = input('Give me a character: ')
    ser.write(str(inv).encode('ascii'))
    myval = ser.readline().decode('ascii')
    return myval

for n in range(1):
    print(sendChar())
    

# terminate the script by calling thesendCharfunction once, and closing the serialport
while True:
    try:
        if ser.in_waiting:
            data = ser.readline()
            if data != 0:
                # Remove line endings
                stripped_data = data.strip()
                
                # split the commas
                split_data = stripped_data.split(',')
                
                # Convert entries to numbers from strings
                mynum1 = float(split_data[0])
                mynum2 = float(split_data[1])
                print(mynum1)
                print(mynum2)
            


            else:
                ser.close()
                
    except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break

