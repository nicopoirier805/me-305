"""@file MotorDriver.py
@brief A motor driver class that encapsulates all of the features associated
        with driving a DC motor connected to the DRV8847 motor driver.
@details This file is intended for use with a DC motor connected to a DRV8847 
            motor driver. The motor is in sleep mode by default. The class 
            activates the motor and controls the diriction the wheels spin.
@date 3/4/21
@author: Nico Poirier
"""

import pyb
import utime

class MotorDriver:
    ''' This class implements a motor driver for the
    ME405 board. '''
    
    def __init__ (self, nSLEEP_pin, IN1_pin, IN2_pin, timer):
        ''' Creates a motor driver by initializing GPIO
        pins and turning the motor off for safety.
        @param nSLEEP_pin   A pyb.Pin object to use as the enable pin. For this lab we use PA15
        @param IN1_pin      A pyb.Pin object to use as the input to half bridge 1.
                            For this lab we use PB4
        @param IN2_pin      A pyb.Pin object to use as the input to half bridge 2. 
                            For this lab we use PB5
        @param timer        A pyb.Timer object to use for PWM generation on
                            IN1_pin and IN2_pin. 
                            For this lab we use timer 3 with freq = 20000.
        @param t3ch1        A pyb.Timer.channel object to use for PWM implementation
                            on IN1_pin
        @param t3ch2        A pyb.Timer.channel object to use for PWM implementation
                            on IN2_pin'''
                                    
        self.nSLEEP_pin     = nSLEEP_pin
        self.IN1_pin        = IN1_pin
        self.IN2_pin        = IN2_pin
        self.timer          = timer
        self.t3ch1          = self.timer.channel(1, pyb.Timer.PWM, pin=self.IN1_pin)
        self.t3ch2          = self.timer.channel(2, pyb.Timer.PWM, pin=self.IN2_pin)
                                  
        print('Creating a motor driver')
        
    def enable (self):
        print('Enabling Motor')
        self.nSLEEP_pin.high()
        
    def disable (self):
        print('Disabling Motor')
        self.nSLEEP_pin.low()
        
    def set_duty (self, duty):
        ''' This method sets the duty cycle to be sent
        to the motor to the given level. Positive values
        cause effort in one direction, negative values
        in the opposite direction.
        @param duty A signed integer holding the duty
            cycle of the PWM signal sent to the motor '''
            
        self.duty = duty
        
        if duty >= 0:
            # if duty is positive, motor forward
            self.IN1_pin.high()
            self.IN2_pin.low()
            self.t3ch1.pulse_width_percent(duty)
            self.t3ch2.pulse_width_percent(0)
            
        elif duty < 0:
            # if duty is negative, motor reverse
            self.IN1_pin.low()
            self.IN2_pin.high()
            self.t3ch1.pulse_width_percent(0)
            self.t3ch2.pulse_width_percent(abs(duty))
            
            
        
if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is importated as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.A15)
    pin_IN1     = pyb.Pin(pyb.Pin.cpu.B4)
    pin_IN2     = pyb.Pin(pyb.Pin.cpu.B5)
    
    # Create the timer object used for PWM generation
    tim         = pyb.Timer(3, freq = 20000)
    
    # Create a motor object passing in the pins and timer
    moe         = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
    
    # Enable the motor driver
    moe.enable()
    start = utime.ticks_ms()
    
    while True:
        try:
            # Set the duty cycle to 10 percent
            moe.set_duty(-50)
            mytime = utime.ticks_ms()
            
            
            if utime.ticks_diff(mytime, start) > 5000:
                moe.disable()
                
            else:
                pass
    
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Ctrl-c has been pressed')
            break

