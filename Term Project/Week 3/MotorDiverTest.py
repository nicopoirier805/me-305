# -*- coding: utf-8 -*-
"""@file MotorDriverTest
@brief
@detials
@date 3/4/21
@author: Nico Poirier
"""

from MotorDriver import MotorDriver
import pyb

if __name__ == '__main__':
    # Adjust the following code to write a test program for your motor class. Any
    # code within the if __name__ == '__main__' block will only run when the
    # script is executed as a standalone program. If the script is importated as
    # a module the code block will not run.
    
    # Create the pin objects used for interfacing with the motor driver
    pin_nSLEEP  = pyb.Pin(pyb.Pin.cpu.PA15)
    pin_IN1     = pyb.Pin(pyb.Pin.cpu.PB4)
    pin_IN2     = pyb.Pin(pyb.Pin.cpu.PB5)
    
    # Create the timer object used for PWM generation
    tim     = pyb.Timer(3, freq = 20000)
    
    # Create a motor object passing in the pins and timer
    moe = MotorDriver(pin_nSLEEP, pin_IN1, pin_IN2, tim)
    
    # Enable the motor driver
    moe.enable()
    
    # Set the duty cycle to 10 percent
    moe.set_duty(10)
    