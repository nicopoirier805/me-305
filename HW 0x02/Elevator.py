# -*- coding: utf-8 -*-
'''@file        Elevator.py
@brief          Simulates an elevator moving between 2 floors
@details        Implements a finite state machine, shown below, to simulate
                an elevator moving between 2 floors
@author         Nico Poirier
@date           1/19/21
'''

import time
import random


def motor_cmd(cmd):
    '''@brief Commands the elevator to move up, down, or stop
       @param cmd The command to give the elevator motor
       @return Motor command
    '''
    if cmd=='DOWN':
        print('Mot down')
    elif cmd=='UP':
        print('Mot up')
    elif cmd=='STOP':
        print('Mot stop')

def floor_1_sensor():
    '''@brief returns True if the elevator is on floor 1
    @param True T or F is randomly assigned to determine if the elevator is on floor 1
    @return True or False
    '''
    return random.choice([True, False]) # randomly returns T or F

def floor_2_sensor():
    '''@brief returns True if the elevator is on floor 2
    @param True T or F is randomly assigned to determine if the elevator is on floor 2
    @return True or False
    '''
    return random.choice([True, False]) # randomly returns T or F

def button_1():
    '''@brief returns True if button 1 is pressed
    @param True T or F is randomly assigned to determine if button 1 is pressed
    @return True or False
    '''
    return random.choice([True, False]) # randomly returns T or F

def button_2():
    '''@brief returns True if button 2 is pressed
    @param True T or F is randomly assigned to determine if button 2 is pressed
    @return True or False
    '''
    return random.choice([True, False]) # randomly returns T or F

# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization goes here
    state = 0 # Initial state is the init state
    
    while True:
        try:
            # main program code goes here
            if state==0:
                # run state 0 (init) code
                print('S0')
                motor_cmd('DOWN') # Command motor to go down
                # If we are on floor 1, stop the elevator and transition to S2
                if floor_1_sensor():
                    state = 1   # Updating state for next iteration
                
            elif state==1:
                # run state 1 (stopped on floor 1) code
                print('S1')
                motor_cmd('STOP') # Command motor to stop
                # If button 2 is pressed transition to state 2 (moving up) code
                if button_2():
                    state=2 # Updating state for next iteration
                
            elif state==2:
                # run state 2 (moving up toward floor 2) code
                print('S2')
                motor_cmd('UP') # Command motor to go up
                # if floor 2 is reached, stop the elevator and transition to state 3
                if floor_2_sensor():
                    state = 3   # Updating state for next iteration
                
            elif state==3:
                # run state 3 (stopped on floor 2) code
                print('S3')
                motor_cmd('STOP') # command motor to stop
                # If button 1 is pressed, move elevator down and transistion to state 0
                if button_1():
                    state=0 # Updating state for next iteration                   
                           
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here
            
            # Slow down execution of FSM so we can see output in console
            time.sleep(0.2)
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('Elevator Simulation has Been Cancelled')
            break

    # Program de-initialization goes  here
    print( 'Thanks!')