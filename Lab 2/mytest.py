# -*- coding: utf-8 -*-
"""
Created on Thu Jan 21 12:57:44 2021

@author: Nico
"""

import pyb

pinC13 = pyb.Pin (pyb.Pin.cpu.C13)

def onButtonPressFCN(IRQ_src):
    print('pushed')
    
 

# Associate the callback function with the pin by setting up an external interrupt.
ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING_RISING,
                       pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)