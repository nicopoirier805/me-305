# -*- coding: utf-8 -*-
"""@file Lab0x02.py
@brief Implements a finite state machine that allows the user to cycle through various LED pulse patterns by clicking the blue button on the Nucleo
@details If the rectangular waveform is selected, the LED will blink on and off at full brightness.
        If the sinusoidal waveform is selected, the LED will oscillate between 100% and 0% brightness.
        If the sawtooth waveform is selected, the LED will gradualy increase from 0% to 100% brightness. 
        
        @image html Lab_2_State_Diagram.png

        
        See source code here:
            https://bitbucket.org/nicopoirier805/me-305/src/master/Lab%202/Lab0x02.py
      
@author: Nico Poirier
@date 1/21/21
"""

import pyb
import utime
from math import sin, pi
import micropython



def onButtonPressFCN(IRQ_src):
    '''@brief Detects if the button is pressed
    @param IRQ_src Detects if external interupt has occured
    @return True
    '''
    global button_pushed
    button_pushed = True

    
def rect_pattern(mytime):
    '''@brief Commands the Nucleo to adjust LED brightness according to
    the rectangular wave pattern
    @param mytime Time since the button was pushed
    @return The duty factor of the LED
    '''
    t2ch1.pulse_width_percent(100*((mytime%1)<0.5))


def sin_pattern(mytime):
    '''@brief Commands the Nucleo to adjust LED brightness according to the sinewave pattern
    @param mytime Time since the button was pushed
    @return The duty factor of the LED
    '''
    t2ch1.pulse_width_percent(100*(0.5*sin((2*pi/10)*mytime)+0.5))  
    
def saw_pattern(mytime):
    '''@brief Commands the Nucleo to adjust LED brightness according to the sawtooth pattern
    @param mytime Time since the button was pushed
    @return The duty factor of the LED
    '''
    t2ch1.pulse_width_percent(100*(mytime%1))
        
            
      
# Main program / test program begin
#   This code only runs if the script is executed as main by pressing play
#   but does not run if the script is imported as a a module
if __name__ == "__main__":
    # Program initialization goes here
    micropython.alloc_emergency_exception_buf(100) # helps with trouble shooting
    state = 'start' # Initial state is the init state
    button_pushed = False # Initial state is button not pushed
    
    pinA5 = pyb.Pin (pyb.Pin.cpu.A5)
    tim2 = pyb.Timer(2, freq = 20000)
    t2ch1 = tim2.channel(1, pyb.Timer.PWM, pin=pinA5)
    
    # Create a pin object for PC13
    pinC13 = pyb.Pin (pyb.Pin.cpu.C13)
    # Associate the callback function with the pin by setting up an external interrupt.
    ButtonInt = pyb.ExtInt(pinC13, mode=pyb.ExtInt.IRQ_FALLING,
                           pull=pyb.Pin.PULL_NONE, callback=onButtonPressFCN)
    # set current time, base time and mytime
    current_time = utime.ticks_ms() 
    base_time = utime.ticks_ms()
    mytime = utime.ticks_diff(current_time, base_time)
    
    # print('Welcome to the Nucleo Finite State Machine!'
    #       'Press button B1, on the Nucleo to cycle through 3 waveforms shown on the LED'
    #       'Waveforms include a rectuangular waveform, sinusoidal waveform, or sawtooth waveform')
    
    while True:
        try:
            # main program code goes here  
            if state == 'start':
                state = 0
                print('Welcome to the Nucleo Finite State Machine!'
                  'Press button B1, on the Nucleo to cycle through 3 waveforms shown on the LED'
                  'Waveforms include a rectuangular waveform, sinusoidal waveform, or sawtooth waveform')                
                
            elif state == 0:
                t2ch1.pulse_width_percent(0)
                
                if button_pushed:
                    print('Rectangular pattern selected, LED will blink on and off at full brightness')
                    state = 1
                    base_time = utime.ticks_ms()
                    button_pushed = False
                    
                else:
                    pass
                
            elif state == 1:
                #update time current time and my time for each time the code runs through
                current_time = utime.ticks_ms() 
                mytime = utime.ticks_diff(current_time, base_time) /1000
                #run rectangular wave through the LED
                rect_pattern(mytime)
                
                if button_pushed:
                    print('Sinusoidal pattern selected, LED will oscillate between 100% and 0% brightness')
                    state = 2
                    base_time = utime.ticks_ms()
                    button_pushed = False
                
                else:
                    pass
            
            elif state == 2:
                #update time current time and my time for each time the code runs through
                current_time = utime.ticks_ms()
                mytime = utime.ticks_diff(current_time, base_time) / 1000
                # run sinusoidal wave through the LED
                sin_pattern(mytime)
                
                if button_pushed:
                    print('Sawtooth pattern selected, LED will gradually increase from 0% to 100% brightness')
                    state = 3
                    base_time = utime.ticks_ms()
                    button_pushed = False
                    
                else:
                    pass
            
            elif state == 3:
                #update time current time and my time for each time the code runs through
                current_time = utime.ticks_ms()
                mytime = utime.ticks_diff(current_time, base_time) / 1000
                # run sawtooth wave through the LED
                saw_pattern(mytime)
                
                if button_pushed:
                    print('Rectangular pattern selected, LED will blink on and off at full brightness')
                    state = 1
                    base_time = utime.ticks_ms()
                    button_pushed = False
                    
                else:
                    pass
                    
            else:
                pass
                # code to run if state number is invalid
                # program should ideally never reach here  
            
            
        except KeyboardInterrupt:
            # This except block catches "Ctrl-C" from the keyboard to end the
            # while(True) loop when desired
            print('You have exited the finite state machine')
            break
        

    # Program de-initialization goes  here
    print( 'Thanks!')         
            