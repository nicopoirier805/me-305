## @file mainpage.py
#
#   @brief Brief doc for mainpage.py
#
#   @details Detailed doc for mainpage.py 
#
#   @mainpage
#   @section section_intro Introduction
#
#   This is a digital portfolio for ME305, a class revolving around mechatronics and the application of python  
#   to accomplish tasks on a nucleo.
#   See individual modules for details.
#
#
#
#   Included modules are:    
#   * \ref page_lab1
#       * \ref sec_fibsource
#   * \ref page_lab2
#       * \ref sec_sourcecode2
#   * \ref page_lab3
#       * \ref sec_sourcecode3
#   * \ref page_termproject
#       * \ref sec_main
#       * \ref sec_UI_dataGen
#       * \ref sec_Lab0xFF
#       * \ref sec_encoder
#       * \ref sec_encoderTask
#       * \ref sec_MotorDriver
#
#
#   @author Nico Poirier
#   @date 3/17/21
#   
#
#
#   @page page_lab1 Lab0x01
#
#   @section sec_fibsource Fibonacci.py Source Code Access
#   You can find the source code for Fibonacci.py here:
#   
#   https://bitbucket.org/nicopoirier805/me-305/src/master/Lab%201/Fibonacci.py
#
#   @section sec_fibdoc Fibbonacci.py Documentation
#   Please see the documentation for Fibonacci.py located in the "Files" folder labeled "Fibonacci.py"
#
#
#
#
#
#   @page page_lab2 Lab0x02  
#
#   @section sec_sourcecode2 Lab0x02.py Source Code Access
#   You can find the source code for Lab0x02.py here:
#   
#   https://bitbucket.org/nicopoirier805/me-305/src/master/Lab%202/Lab0x02.py    
#
#   @section sec_doc2 Lab0x02.py Documentation
#   Please see the documentation for Lab0x02.py located in the "Files" foler labeled "Lab0x02.py"
#   
#   The state diagram is located below:   
#   @image html Lab_2_State_Diagram.png width=70%
#
#   @section sec_instructions_lab2 Instructions
#   Download the file "Lab0x02.py" and run it on the nucleo. The user should be able to cycle
#   through 3 different LED patterns.
#
#
#
#
#
#   @page page_lab3 Lab0x03
#
#   @section sec_sourcecode3 Lab0x03.py Source Code Access
#   You can find the source code for lab0x03.py here:
#   
#   https://bitbucket.org/nicopoirier805/me-305/src/master/Lab%203/Lab0x03.py
#
#   @section sec_doc3 Lab0x03.py Documentation
#   Please see the documentation for Lab0x03.py located in the "Files" folder labeled "Lab0x03.py"
#   
#   The state diagram is located below:
#   @image html Lab0x03_diagram.png width=70%
#
#   A video of the working code can be found here:
#   
#   https://youtu.be/mhfxuSE82o0
#
#
#
#
#   @page page_termproject Term Project
#       
#   @section sec_main Week 1: main.py
#   You can find the source code for "main.py" here:
#   
#   https://bitbucket.org/nicopoirier805/me-305/src/master/Term%20Project/Week%201/main.py
#
#   Please see the documentation for main.py
#
#   @section sec_UI_dataGen Week 1: UI_dataGen.py
#   You can find the source code for "UI_dataGen.py" here:
#   
#   https://bitbucket.org/nicopoirier805/me-305/src/master/Term%20Project/Week%201/UI_dataGen.py
#
#   Please see the documentation for UI_dataGen.py
#
#   The state diagram is shown below:
#   @image html UI_datagen.png width=70%
#
#   @section sec_Lab0xFF Week 1: Lab0xFF.py
#   You can find the source code for "Lab0xFF.py" here:
#   
#   https://bitbucket.org/nicopoirier805/me-305/src/master/Term%20Project/Week%201/Lab0xFF.py
#
#   Please see the documentation for Lab0xFF.py
#
#   The task diagram between week 1 files is shown below:
#   @image html Datagen_taskdiagram.png width=70%
#
#
#   @section sec_encoder Week 2: encoder.py
#   You can find the source code for "encoder.py" here:
#
#   https://bitbucket.org/nicopoirier805/me-305/src/master/Term%20Project/Week%202/encoder.py
#
#   Please see the documentation for encoder.py
#
#
#   @section sec_encoderTask Week 2: encoderTask.py
#   You can find the source code for "encoderTask.py" here:
#   
#   https://bitbucket.org/nicopoirier805/me-305/src/master/Term%20Project/Week%202/encoderTask.py
#   
#   Please see the documentation for encoderTask.py
#
#   The task diagram for the week 2 files is shown below:
#
#
#   @section sec_MotorDriver Week 3: MotorDriver.py
#   
#   You can find the source code for "MotorDriver.py" here:
#
#   https://bitbucket.org/nicopoirier805/me-305/src/master/Term%20Project/Week%203/MotorDriver.py
#
#   Please see the documentation for MotorDriver.py
#
#   @section sec_term_diagram Task Diagram for all Term Project Files
#   I was not able to complete working versions of all the files intended for this 
#   assignment. If I had all of the files the complete task diagram would look like
#   the one shown below:
#   @image html 
#
#   
#